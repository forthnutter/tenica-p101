﻿
**************************************
 Tenica P-101 Moving Text LED Sign
**************************************
This is project is a fork of Steven T. Snyder Tenica P-101 Disassembly Project

I have added a hardware directory for reverse engineered a schematics of the PCB's


Project folders:
/hardware -- Schematics
/util     -- Utility scripts and software
/dis      -- The raw disassembly code, and the code I have commented
/ref      -- Reference material such as datasheets and code manuals
/bin      -- Binary PROM dump



The schematics were drawn using DesignSpark http://www.designspark.com because its free and
simple to use.


forthnutter
